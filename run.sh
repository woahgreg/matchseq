#!/bin/bash

# constants

BIN=matchseq

# build

mkdir -p build
cd build/
cmake ..
make
cp $BIN ..
cd ..

# run

echo
echo "ACACGTCAε" | ./$BIN -T:ACGT -x:1 -y:2

echo
echo "AAGTACGTGCAGTGAGTAGTAGACCTGACGTAGACCGATATAAGTAGCTAε" | \
./$BIN -T:AGTA -x:5 -y:7

echo
echo "AAGTACGTGCAGTGAGTAGTAGACCTGACGTAGACCGATATAAGTAGCTAε" | \
./$BIN -T:AGTA -x:0 -y:0

echo
