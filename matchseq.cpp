//
//  main.cpp
//  matchseq
//
//  Created by Greg Grant on 5/18/19.
//  Copyright © 2019 Gregory Grant. All rights reserved.
//

#include <cstdio>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool ParseArguments(
  const int argc,
  const char * argv[],
  unordered_map<string, string>& ParameterMap)
{
  // parse command line arguments
  for (int iParam = 1; iParam < argc; ++iParam)
  {
    string ParameterString = argv[iParam];
    
    string Delim = ":";
    auto DelimPos = ParameterString.find(Delim);
    string Key = ParameterString.substr(0, DelimPos);
    string Value = ParameterString.substr(DelimPos + 1);
    
    // check that input argument is in map of expected arguments
    auto iMap = ParameterMap.find(Key);
    if (iMap == ParameterMap.end())
    {
      cerr
      << "ERROR: Unexpected argument received: " << Key
      << endl;
      
      return false;
    }
    else if (!ParameterMap[Key].empty())
    {
      cerr
      << "ERROR: Repeated argument received: " << Key
      << endl;
      
      return false;
    }
    else
    {
      ParameterMap[Key] = Value;
    }
  }
  
  return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main(int argc, const char * argv[])
{
  const int NumParameters = 4;
  
  // check number of arguments
  if (argc == 1)
  {
    cout
      << "Usage:"
      << endl
      << "  -T: [string] Target nucleotide sequence,"
      << endl
      << "  -x: [unsigned, [0, n]] Number of preceding nucleotides,"
      << endl
      << "  -y: [unsigned, [0, n]] Number of succeeding nucleotides."
      << endl
      << "Example usage:"
      << endl
      << "  echo \"ACACGTCAε\" | matchseq -T:ACGT -x:1 -y:2"
      << endl
      << "Output:"
      << endl
      << "  C ACGT CA"
      << endl;
    
    return 0;
  }
  else if (argc != NumParameters)
  {
    cerr
      << "ERROR: Expected " << NumParameters - 1
      << " arguments, received " << argc - 1
      << " arguments"
      << endl;
    
    return -1;
  }
  
  vector<string> Keys = { "-T", "-x", "-y" };
  unordered_map<string, string> ParameterMap = {
    {Keys[0], ""},
    {Keys[1], ""},
    {Keys[2], ""}
  };
  
  if (!ParseArguments(argc, argv, ParameterMap))
  {
    return -1;
  }
  
  // set parameter variables
  const string T = ParameterMap[Keys[0]];
  const int x = stoi(ParameterMap[Keys[1]]);
  const int y = stoi(ParameterMap[Keys[2]]);
  
  // remove spaces if x or y are 0
  string xSubSpace = (x == 0) ? "" : " ";
  string ySubSpace = (y == 0) ? "" : " ";
  
  // buffer minimum number of characters
  unsigned BufferSize = x + T.size() + y + 1;
  
  string S;
  char Char;
  vector<size_t> PositionBuffer;
  
  int IndexChange = -1;
  bool FoundEscapeCharacter = false;
  string EscapeCharacterString;
  
  // capture stdin
  while (!FoundEscapeCharacter && cin >> Char)
  {
    // do this only if we are not on the escape char
    if (!FoundEscapeCharacter)
    {
      // check if character is an alphabetic letter
      // if yes, add to sequence string
      // if no, add to escape character string and check if the string is the escape character
      if (isalpha(Char) == 0)
      {
        EscapeCharacterString += Char;
        if (EscapeCharacterString == "ε")
        {
          FoundEscapeCharacter = true;
        }
        else
        {
          continue;
        }
      }
      else
      {
        S += Char;
        
        // erase first value to keep string size at BufferSize
        if (S.size() > BufferSize)
        {
          S.erase(0, 1);
          
          if (!FoundEscapeCharacter)
          {
            ++IndexChange;
          }
        }
      }
    }
    
    // loop buffer for possible outputs
    if (!PositionBuffer.empty())
    {
      for (int i = 0; i < PositionBuffer.size(); ++i)
      {
        // decrement position in the case where S starts to shift
        // which is the second time we take a substring of S rather than S
        // itself
        if (IndexChange >= 0 && !FoundEscapeCharacter)
        {
          --PositionBuffer[i];
        }
        
        size_t Position = PositionBuffer[i];
        
        if (S.size() - Position - 1 == T.size() + y || FoundEscapeCharacter)
        {
          string xSub =
            (Position < x) ?
            S.substr(0, Position) :
            S.substr(Position - x, x);
          
          string SSub = S.substr(Position, T.size());
          
          size_t yPos = Position + T.size();
          int NumRemainingChars = S.size() - yPos;
          string ySub =
            (NumRemainingChars < y) ?
            S.substr(yPos, NumRemainingChars) :
            S.substr(yPos, y);
          
          // output and remove from buffer
          cout << xSub << xSubSpace << SSub << ySubSpace << ySub << endl;
          PositionBuffer[i] = -1; // mark for deletion
        }
        
        if (Position == -1)
        {
          continue;
        }
      }
      
      // loop buffer and erase select elements
      auto iBuffer = PositionBuffer.begin();
      while (iBuffer != PositionBuffer.end())
      {
        if (*iBuffer == -1)
        {
          iBuffer = PositionBuffer.erase(iBuffer);
        }
        else
        {
          ++iBuffer;
        }
      }
    }
    
    // search last T.size() characters for T
    size_t Position = S.find(T, S.size() - T.size());
    if (Position != string::npos)
    {
      PositionBuffer.push_back(Position);
    }
  }
  
  return 0;
}
